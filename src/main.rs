mod cf;

use std::time::{Duration, Instant};

use config::ConfigError;

use crate::cf::types::DNSRecord;

#[derive(serde::Deserialize)]
struct Settings {
    zones: Vec<String>,
    records: Vec<String>,
    api_token: String,
    frequency: u64,
    ip_lookup_url: String,
}

/// Parse configuration using `config`
fn parse_cfg() -> Result<Settings, config::ConfigError> {
    // Build Config
    let cfg = config::Config::builder()
        .add_source(config::File::with_name(".cfddns"))
        .add_source(config::Environment::with_prefix("CFDDNS"))
        .set_default("frequency", 300)?
        .set_default("ip_lookup_url", "https://ipinfo.io/ip")?
        .build()?;
    // Parse Settings from resulting Config
    let settings = cfg.try_deserialize::<Settings>()?;
    Ok(settings)
}

fn update_ddns(api: &cf::api::CloudflareAPI, cfg: &Settings) {
    // Fetch DNS zones
    let zones = api.list_zones();

    // Fetch current public IP
    let ip = match reqwest::blocking::get(&cfg.ip_lookup_url) {
        Ok(r) => {
            if r.status().as_u16() != 200 {
                println!("Error fetching IP: {}", r.status());
                return;
            }
            r.text().unwrap()
        }
        Err(e) => {
            println!("Error performing get request to fetch IP: {}", e);
            return;
        }
    };

    // Iterate over zones to match
    for zone in &cfg.zones {
        // Get zone ID
        let mut zone_id: Option<String> = None;
        for z in &zones {
            if z.name == *zone || z.id == *zone {
                zone_id = Some(z.id.clone());
            }
        }
        let zone = match zone_id {
            None => {
                println!("Unable to find match for zone {}, skipping...", zone);
                continue;
            }
            Some(ref z) => z,
        };
        // Check if listed records exist in zone
        let mut records = api.list_dns_records(&zone);
        for rec in &cfg.records {
            let mut matched = false;
            for record in &mut records {
                if record.name == *rec {
                    // Update record
                    matched = true;
                    record.content = ip.clone();
                    api.update_dns_record(&record.zone_id, &record);
                    println!("Updated {:?}", rec);
                    break;
                }
            }
            if !matched {
                // Create record
                let record = DNSRecord {
                    content: ip.clone(),
                    name: rec.clone(),
                    proxied: None,
                    record_type: "A".to_string(),
                    comment: None,
                    created_on: None,
                    id: None,
                    locked: None,
                    modified_on: None,
                    proxiable: None,
                    tags: vec![],
                    ttl: cfg.frequency as usize,
                    zone_id: zone_id.clone().unwrap(),
                    zone_name: None,
                };
                api.create_dns_record(&record.zone_id, &record);
                println!("Created {:?}", &rec);
            }
        }
    }
}

fn main() {
    let cfg = match parse_cfg() {
        Ok(c) => c,
        Err(e) => {
            panic!("Unable to parse config: {}", e)
        }
    };
    let api = cf::api::CloudflareAPI::with_auth(cfg.api_token.clone());

    let delay = std::time::Duration::from_secs(cfg.frequency.clone());

    loop {
        update_ddns(&api, &cfg);
        std::thread::sleep(delay);
    }
}
