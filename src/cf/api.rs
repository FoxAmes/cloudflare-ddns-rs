use crate::cf::types::*;

use reqwest::{header::HeaderMap, Method};

const CF_API_LATEST_VERSION: &str = "v4";

#[derive(serde::Deserialize, serde::Serialize)]
pub struct APICodedMessage {
    code: isize,
    message: String,
}

#[derive(serde::Deserialize, serde::Serialize)]
pub struct APIResponse<T> {
    result: T,
    errors: Vec<APICodedMessage>,
    messages: Vec<APICodedMessage>,
    success: bool,
}

pub type APIResult<T> = Result<APIResponse<T>, String>;

pub struct CloudflareAPI {
    api_token: String,
    client: reqwest::blocking::Client,
    api_version: String,
}

impl CloudflareAPI {
    pub fn with_auth(api_token: String) -> Self {
        Self::with_auth_versioned(api_token, CF_API_LATEST_VERSION.to_string())
    }

    pub fn with_auth_versioned(api_token: String, api_version: String) -> Self {
        // TODO: Should properly handle errors below
        let client = reqwest::blocking::Client::builder()
            .https_only(true)
            .build()
            .unwrap();
        Self {
            api_token,
            client,
            api_version,
        }
    }

    /// General purpose API request to the Cloudflare API
    fn request<T>(
        &self,
        path: String,
        method: Method,
        headers: Option<HeaderMap>,
        body: Option<String>,
    ) -> APIResult<T>
    where
        T: serde::de::DeserializeOwned,
    {
        // Assemble URL
        let url = format!(
            "https://api.cloudflare.com/client/{}/{}",
            self.api_version, path
        );

        // Format headers and set applicable parameters
        let mut req = self.client.request(method, url);
        req = req.header(
            "Authorization",
            format!("Bearer {}", self.api_token.clone()),
        );
        req = req.header("Accept", "application/json");
        if body.is_some() {
            req = req.header("Content-Type", "application/json");
        }
        if let Some(headers) = headers {
            req = req.headers(headers);
        }
        if let Some(body) = body {
            req = req.body(body);
        }

        // Build and perform request
        let req = match req.build() {
            Ok(r) => r,
            Err(e) => {
                panic!("Error building API request: {}", e);
            }
        };
        let resp = match self.client.execute(req) {
            Ok(r) => r,
            Err(e) => {
                panic!("Error executing API request: {}", e);
            }
        };

        // Handle errors
        match resp.status().as_u16() {
            200..=399 => {}
            400..=599 => {
                return Err(format!(
                    "Error from API: {} ({}): {}",
                    resp.status().as_str(),
                    resp.status().canonical_reason().unwrap_or("Unknown"),
                    resp.text().unwrap()
                ))
            }
            _ => {}
        }

        // Parse response
        let text = resp.text().unwrap();
        let resp: APIResponse<T> = match serde_json::from_str(&text) {
            Ok(j) => j,
            Err(e) => {
                println!("{}", &text);
                panic!("Error marshalling JSON: {}", e);
            }
        };

        Ok(resp)
    }

    /// Convenience wrapper to perform a POST request
    fn post<T>(
        &self,
        path: String,
        headers: Option<HeaderMap>,
        body: Option<String>,
    ) -> APIResult<T>
    where
        T: serde::de::DeserializeOwned,
    {
        self.request(path, Method::POST, headers, body)
    }

    /// Convenience wrapper to perform a GET request
    fn get<T>(&self, path: String, headers: Option<HeaderMap>) -> APIResult<T>
    where
        T: serde::de::DeserializeOwned,
    {
        self.request(path, Method::GET, headers, None)
    }

    /// Convenience wrapper to perform a PUT request
    fn put<T>(&self, path: String, headers: Option<HeaderMap>, body: Option<String>) -> APIResult<T>
    where
        T: serde::de::DeserializeOwned,
    {
        self.request(path, Method::PUT, headers, body)
    }

    /// Convenience wrapper to perform a DELETE request
    fn delete<T>(&self, path: String, headers: Option<HeaderMap>) -> APIResult<T>
    where
        T: serde::de::DeserializeOwned,
    {
        self.request(path, Method::DELETE, headers, None)
    }

    // Begin specific endpoint implementations
    pub fn create_dns_record(&self, zone_id: &String, record: &DNSRecord) -> DNSRecord {
        let resp = self.post::<DNSRecord>(
            format!("zones/{}/dns_records", zone_id),
            None,
            Some(serde_json::to_string(&record).unwrap()),
        );
        resp.unwrap().result
    }
    pub fn get_dns_record(&self, zone_id: &String, record_id: String) -> DNSRecord {
        let resp =
            self.get::<DNSRecord>(format!("zones/{}/dns_records/{}", zone_id, record_id), None);
        resp.unwrap().result
    }
    pub fn list_dns_records(&self, zone_id: &String) -> Vec<DNSRecord> {
        let resp = self.get::<Vec<DNSRecord>>(format!("zones/{}/dns_records", zone_id), None);
        resp.unwrap().result
    }
    pub fn update_dns_record(&self, zone_id: &String, record: &DNSRecord) -> DNSRecord {
        let resp = self.put::<DNSRecord>(
            format!(
                "zones/{}/dns_records/{}",
                zone_id,
                record.id.as_ref().unwrap()
            ),
            None,
            Some(serde_json::to_string(&record).unwrap()),
        );
        resp.unwrap().result
    }
    pub fn list_zones(&self) -> Vec<Zone> {
        let resp = self.get::<Vec<Zone>>("zones".to_string(), None);
        resp.unwrap().result
    }
}
