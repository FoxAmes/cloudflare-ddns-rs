/// A DNS record
#[derive(Debug, Clone, PartialEq, Eq, serde::Deserialize, serde::Serialize)]
pub struct DNSRecord {
    /// A valid IPv4 address.
    ///
    /// Example: `198.51.100.4`
    pub content: String,
    /// DNS record name (or @ for the zone apex) in Punycode.
    ///
    /// `<= 255 characters`
    ///
    /// Example: `example.com`
    pub name: String,
    /// Whether the record is receiving the performance and security benefits of Cloudflare.
    ///
    /// Example: `false`
    pub proxied: Option<bool>,
    /// Record type.
    ///
    /// Allowed value: `A`
    ///
    /// Example: `A`
    #[serde(rename = "type")]
    pub record_type: String,
    /// Comments or notes about the DNS record. This field has no effect on DNS responses.
    ///
    /// Example: `Domain verification record`
    pub comment: Option<String>,
    /// When the record was created.
    ///
    /// Example: `2014-01-01T05:20:00.12345Z`
    pub created_on: Option<String>,
    /// Identifier
    ///
    /// `<= 32 characters`
    ///
    /// Example: `023e105f4ecef8ad9ca31a8372d0c353`
    pub id: Option<String>,
    /// Whether this record can be modified/deleted (true means it's managed by Cloudflare).
    ///
    /// Example: `false`
    pub locked: Option<bool>,
    /// When the record was last modified.
    ///
    /// Example: `2014-01-01T05:20:00.12345Z`
    pub modified_on: Option<String>,
    /// Whether the record can be proxied by Cloudflare or not.
    ///
    /// Example: `true`
    pub proxiable: Option<bool>,
    /// Custom tags for the DNS record. This field has no effect on DNS responses.
    pub tags: Vec<String>,
    /// Time To Live (TTL) of the DNS record in seconds. Setting to 1 means 'automatic'. Value must be between 60 and 86400, with the minimum reduced to 30 for Enterprise zones.
    pub ttl: usize,
    /// Identifier
    ///
    /// `<= 32 characters`
    ///
    /// Example: `023e105f4ecef8ad9ca31a8372d0c353`
    pub zone_id: String,
    /// The domain of the record.
    ///
    /// Example: `example.com`
    pub zone_name: Option<String>,
}

/// A DNS zone
#[derive(Debug, Clone, PartialEq, Eq, serde::Deserialize, serde::Serialize)]
pub struct Zone {
    /// The last time proof of ownership was detected and the zone was made active
    ///
    /// Example: `2014-01-02T00:01:00.12345Z`
    pub activated_on: String,
    /// When the zone was created
    ///
    /// Example: `2014-01-01T05:20:00.12345Z`
    pub created_on: String,
    /// The interval (in seconds) from when development mode expires (positive integer) or last expired (negative integer) for the domain. If development mode has never been enabled, this value is 0.
    ///
    /// Example: `7200`
    pub development_mode: usize,
    /// Identifier
    ///
    /// `<= 32 characters`
    ///
    /// Example: `023e105f4ecef8ad9ca31a8372d0c353`
    pub id: String,
    /// When the zone was last modified
    ///
    /// Example: `2014-01-01T05:20:00.12345Z`
    pub modified_on: String,
    /// The domain name
    ///
    /// `<= 253 characters`
    ///
    /// Example: `example.com`
    ///
    /// Match pattern: `^([a-zA-Z0-9][\-a-zA-Z0-9]*\.)+[\-a-zA-Z0-9]{2,20}$`
    pub name: String,
    /// DNS host at the time of switching to Cloudflare
    ///
    /// `<= 50 characters`
    ///
    /// Example: `NameCheap`
    pub original_dnshost: Option<String>,
    /// Original name servers before moving to Cloudflare
    /// Notes: Is this only available for full zones?
    ///
    /// Example: `["ns1.originaldnshost.com","ns2.originaldnshost.com"]`
    pub original_name_servers: Option<Vec<String>>,
    /// Registrar for the domain at the time of switching to Cloudflare
    /// Example: `GoDaddy`
    pub original_registrar: Option<String>,
    /// An array of domains used for custom name servers. This is only available for Business and Enterprise plans.
    ///
    /// Example: `["ns1.example.com","ns2.example.com"]`
    pub vanity_name_servers: Option<Vec<String>>,
}

/// An IP Address
#[derive(Debug, Clone, PartialEq, Eq, serde::Deserialize, serde::Serialize)]
pub struct IPAddress {
    /// Example: `15169`
    asn: String,
    /// Example: `US`
    asn_location: String,
    /// Example: `GOOGLE`
    asn_name: String,
    /// Example: `Google LLC`
    asn_org_name: String,
    /// Example: `8.8.8.8`
    ip: String,
    /// Example: `IPv4`
    ip_version: String,
    /// Example: `GB`
    location: String,
    /// Example: `United Kingdom`
    location_nme: String,
}
