# Cloudflare Dynamic DNS Agent
This is a simple application to provide Dynamic DNS (DDNS) through Cloudflare. The application will, on a configurable interval:

1. Fetch the current public IP address of the system
2. Send API requests to Cloudflare to update specified DNS records accordingly

## Configuration
Configuration can be provided either via a config file or environment variables.
### Example
|**config key**|**env var**|**type**|**notes**|**default**|
|-|-|-|-|-|
|zones|CFDDNS_ZONES|`Vec<String>`|List of zones to check for records||
|records|CFDDNS_RECORDS|`Vec<String>`|List of record names to create/update||
|api_token|CFDDNS_API_TOKEN|`String`|API token used to authenticate with Cloudflare's API. Must have access to edit records in the given zones||
|frequency|CFDDNS_FREQUENCY|`u64`|Duration (in seconds) between updates. Also used to set the TTL of the record, so must be a valid TTL|`300`|
|ip_lookup_url|CFDDNS_IP_LOOKUP_URL|`String`|URL to use to query public IP address. Must return the IP address in plain text as the only content of the body of the response|`https://ipinfo.io/ip`|