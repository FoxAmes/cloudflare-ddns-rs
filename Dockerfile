FROM alpine:latest

RUN apk update && \
    apk add dumb-init

RUN adduser -u 913 -D app
USER app

ADD target/x86_64-unknown-linux-musl/release/cloudflare-ddns-rs /usr/bin/cloudflare-ddns-rs

VOLUME /config

ENTRYPOINT ["/usr/bin/dumb-init"]

CMD ["/usr/bin/cloudflare-ddns-rs"]